from dataclasses import dataclass, fields
from decimal import Decimal

from django.db.models import Prefetch

from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response

from catalog.models import Product, Warehouse, ProductImage
from orders.cart import SessionCart
from orders.service import (check_user_contact_data,
                            create_user_contact_data,
                            create_order,
                            create_order_product,
                            send_email_managers_about_order)
from orders.serializers import CartSerializer


@dataclass()
class PostCartItem():
    """
    Проверка данных корзины, полученных из POST запроса
    При наличии первой ошибки возвращает self.status == False.
    Описание первой ошибки в self.message.
    Если ошибок нет то данные корзины в атрибутах.
    """

    product_id: int = None
    warehouse_id: int = None
    quantity: int = None
    status: bool = True
    message: str = ''

    def __iter__(self):
        for field in fields(self):
            if field.type == int:
                yield field

    def _check_none(self):
        if self.status:
            for field in self:
                if getattr(self, field.name) is None:
                    self.status = False
                    self.message = f'{field.name} no data'
                    break

    def _check_type(self):
        if self.status:
            for field in self:
                try:
                    setattr(self, field.name, int(getattr(self, field.name)))
                except ValueError:
                    self.status = False
                    self.message = f'{field.name} not integer type'
                    break

    def _check_value(self):
        if self.status:
            for field in self:
                if getattr(self, field.name) < 0:
                    self.status = False
                    self.message = f'{field.name} value < 0'
                    break

    def check_post_data(self, data):
        self.product_id = data.get('product_id')
        self.warehouse_id = data.get('warehouse_id')
        self.quantity = data.get('quantity')
        self._check_none()
        self._check_type()
        self._check_value()


@api_view(['POST'])
def cart_add_product(request: Request):
    cart_item = PostCartItem()
    cart_item.check_post_data(data=request.data)

    if cart_item.status:                                        # Данные в корзине корректны
        cart = SessionCart(request)
        cart.add_item(product_id=cart_item.product_id,
                      warehouse_id=cart_item.warehouse_id,
                      quantity=cart_item.quantity)
        message = 'To cart add product'
        response = Response(data={'status': cart_item.status, 'message': message}, status=200)
    else:                                                       # Данные в корзине не корректны
        response = Response(data={'status': cart_item.status, 'message': cart_item.message}, status=400)

    return response


@api_view(['POST'])
def cart_change_product_quantity(request: Request):
    cart_item = PostCartItem()
    cart_item.check_post_data(data=request.data)

    if cart_item.status:                                        # Данные в корзине корректны
        cart = SessionCart(request)
        cart.change_item(product_id=cart_item.product_id,
                         warehouse_id=cart_item.warehouse_id,
                         quantity=cart_item.quantity)
        message = 'In cart change product quantity'
        response = Response(data={'status': cart_item.status, 'message': message}, status=200)
    else:                                                       # Данные в корзине не корректны
        response = Response(data={'status': cart_item.status, 'message': cart_item.message}, status=400)

    return response


@api_view(['POST'])
def cart_delete_product(request: Request):
    cart_item = PostCartItem()
    cart_item.check_post_data(data=request.data)

    if cart_item.status:                                        # Данные в корзине корректны
        cart = SessionCart(request)
        cart.delete_item(product_id=cart_item.product_id,
                         warehouse_id=cart_item.warehouse_id)
        message = f'In cart delete product_id {cart_item.product_id}'
        response = Response(data={'status': cart_item.status, 'message': message}, status=200)
    else:                                                       # Данные в корзине не корректны
        response = Response(data={'status': cart_item.status, 'message': cart_item.message}, status=400)

    return response


@api_view(['POST'])
def cart_clear(request: Request):
    cart = SessionCart(request)
    cart.clear_cart()
    message = 'Cart clear'
    response = Response(data={'status': True, 'message': message}, status=200)
    return response


@api_view(['POST'])
def submit_order(request: Request):
    """
    Создание заказа. После успеха очищаем корзину.
    """

    data = request.data
    cart = SessionCart(request)
    cart_list_products = cart.cart_to_dict()['products']

    if cart_list_products:                                                              # Корзина не пуста.
        user_contact_data = check_user_contact_data(data)

        if user_contact_data['status']:                                                 # Данные пользователя корректны.
            order = create_order()                                                      # Создание заказа 'order'
            create_order_product(order, cart_list_products)
            create_user_contact_data(user_contact_data['message'], order)               # Создание пользователя с заказом 'order'
            send_email_managers_about_order(order)                                      # Отправка на почту извещение менеджерам

            response = Response(data={'status': True,
                                      'message': 'Order created successfully'
                                      },
                                status=200)
            cart.clear_cart()                                                           # Очистка корзины

        else:                                                                           # Данные пользователя не корректны.
            response = Response(data={'status': False,
                                      'message': 'User data incorrect'
                                      },
                                status=400)

    else:                                                                               # Пустая корзина.
        response = Response(data={'status': False,
                                  'message': 'Cart empty'
                                  },
                            status=400)

    return response


@api_view(['GET'])
def cart_detail(request: Request):
    cart = SessionCart(request).cart_to_dict()

    products = Product.objects.prefetch_related(
        Prefetch(
            'images',
            queryset=ProductImage.objects.order_by('thumbnail').exclude(thumbnail='')
        ),
        'stocks',
        )

    warehouses = Warehouse.objects.all()

    cart_list = []
    total = Decimal('0')

    for cart_item in cart.get('products'):
        product = products.get(pk=int(cart_item['product_id']))

        price = product.price
        warehouse = warehouses.get(pk=int(cart_item['warehouse_id']))
        quantity = int(cart_item['quantity'])
        amount = round(price * quantity, 2).quantize(Decimal('1.00'))

        cart_list.append({'product': product,
                          'price': price,
                          'warehouse': warehouse,
                          'quantity': quantity,
                          'amount': amount})
        total += amount

    serializer = CartSerializer(cart_list, many=True)

    return Response(serializer.data)
