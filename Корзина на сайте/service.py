from dataclasses import dataclass, fields
import datetime
import logging
from smtplib import SMTPResponseException

from django.core.mail import mail_managers
from django.db import IntegrityError

from catalog.models import Product, Warehouse
from orders.models import Contact, Order, OrderProduct

logger = logging.getLogger(__name__)


def send_email_managers_about_order(order: Order):
    """ Отправляет заявку на e-mail менеджеров. """
    try:
        subject = f'Новая заявка № {order.number}'
        body = ''
        mail_managers(subject, body)
    except SMTPResponseException:
        logger.warning('send_email_managers_about_order error')
        raise IntegrityError('Не удалось отправить email менеджерам.')


@dataclass()
class UserContactData():
    """
    Класс данных пользователя, полученных из POST запроса
    При наличии первой ошибки возвращает self.status == False.
    Описание первой ошибки в self.message.
    Если ошибок нет то данные пользователя в атрибутах класса.
    """

    name: str = None
    email: str = None
    phone: str = None
    status: bool = True
    message: str = ''

    def __iter__(self):
        for field in fields(self):
            if field.type == str and field.name != 'message':
                yield field

    def _check_none(self):
        if self.status:
            for field in self:
                if getattr(self, field.name) is None:
                    self.status = False
                    self.message = f'{field.name} no data'
                    break

    def _check_value(self):
        if self.status:
            for field in self:
                if not getattr(self, field.name):
                    self.status = False
                    self.message = f'{field.name} empty value'
                    break

    def check_post_data(self, data: dict):
        self.name = data.get('name')
        self.email = data.get('email')
        self.phone = data.get('phone')
        self._check_none()
        self._check_value()


def make_order_number() -> str:
    """
    Возвращает сгенерированный номер заказа.
    Генерирует последовательный номер каждую миллисекунду.
    """

    number = ''
    current_date = datetime.datetime.now()
    number = f"{current_date.strftime('%Y%m%d')}-{current_date.strftime('%f')}"

    return number


def check_user_contact_data(data: dict) -> dict:
    """ Проверка данных пользователя из POST запроса. """

    result = {}
    user_contact_data = UserContactData()
    user_contact_data.check_post_data(data)
    if user_contact_data.status:
        result['status'] = True
        result['message'] = user_contact_data
    else:
        result['status'] = False
        result['message'] = 'Error validation user contact data'

    return result


def create_user_contact_data(user_data: UserContactData, order: Order) -> Contact:
    """ Создание анонимного пользователя. """

    try:
        obj = Contact.objects.create(
            name=user_data.name,
            email=user_data.email,
            phone=user_data.phone,
            order=order,
        )
    except IntegrityError:
        logger.warning('create_user_contact_data error')
        raise IntegrityError('Не удалось создать пользователя')

    return obj


def create_order() -> Order:
    """ Создание заказа. """

    for i in range(10):
        try:
            obj = Order.objects.create(number=make_order_number())
        except IntegrityError:
            continue
        else:
            break
    else:
        logger.warning('create_order error')
        raise IntegrityError('Не удалось создать заказ с уникальным номером (потрачено 10 попыток)')

    return obj


def create_order_product(order: Order, cart: list) -> list[OrderProduct]:
    """
    Создание товаров в заказе 'order'.
    Товары получаем из корзины пользователя.
    """

    products = Product.objects.all()
    warehouses = Warehouse.objects.all()

    order_product_list = []

    for cart_item in cart:
        try:
            product = products.get(pk=int(cart_item['product_id']))
            price = product.price
            warehouse = warehouses.get(pk=int(cart_item['warehouse_id']))
            quantity = int(cart_item['quantity'])
        except KeyError as exception:
            logger.warning(f'create_order_product error:{exception}')
            raise KeyError('Не удалось создать товар к заказу')

        order_product_list.append(OrderProduct(order=order,
                                               product=product,
                                               warehouse=warehouse,
                                               quantity=quantity,
                                               price=price))

    objs = OrderProduct.objects.bulk_create(order_product_list)

    return objs
