from django.db import models
from catalog.models import Product, Warehouse


class Order(models.Model):
    """ Заказы. """

    class Status(models.IntegerChoices):
        NEW = 0
        PROCESSING = 10
        COMPLETED = 20
        CANCELED = 30

    number = models.CharField(
        verbose_name="Номер заказа",
        max_length=15,
        unique=True,
        db_index=True,
    )
    created_at = models.DateTimeField(
        verbose_name='Дата создания',
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name='Дата обновления',
        auto_now=True
    )
    status = models.PositiveIntegerField(
        verbose_name='Статус',
        help_text='Статус заказа',
        choices=Status.choices,
        default=Status.NEW,
        db_index=True,
    )

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return f'{self.number}'

    def __repr__(self):
        return f'[{self.pk}] number={self.number}'


class OrderProduct(models.Model):
    """ Товары в заказах. """

    order = models.ForeignKey(
        verbose_name='Заказ',
        help_text='Ссылка на заказ',
        to=Order,
        on_delete=models.CASCADE,
        related_name='products',
    )
    product = models.ForeignKey(
        verbose_name='Товар',
        help_text='Ссылка на товар',
        to=Product,
        on_delete=models.CASCADE,
        related_name='products',
    )
    warehouse = models.ForeignKey(
        verbose_name='Склад',
        help_text='Ссылка на склад',
        to=Warehouse,
        on_delete=models.CASCADE,
        related_name='products',
    )
    quantity = models.PositiveIntegerField(
        verbose_name='Кол-во единиц продукции',
    )
    price = models.DecimalField(
        verbose_name='Цена',
        help_text='Цена за единицу товара',
        max_digits=10,      # Максимальное общее количество цифр, включая значения после запятой
        decimal_places=2,   # Количество цифр после запятой
        null=True,
    )

    class Meta:
        verbose_name = 'Товары в заказе'
        verbose_name_plural = 'Товары в заказах'

    def __str__(self):
        return f'{self.order}'

    def __repr__(self):
        return f'[{self.pk}] order={self.order}'


class Contact(models.Model):
    """ Контакты анонимных пользователей. """

    name = models.CharField(
        max_length=50,
        verbose_name='Имя',
        help_text='Имя пользователя',
        null=True,
        blank=True,
    )
    email = models.EmailField(
        verbose_name='Эл. почта',
        null=False,
    )
    phone = models.CharField(
        max_length=20,
        verbose_name='Телефон',
        help_text='Телефонный номер',
        null=False,
    )
    order = models.OneToOneField(
        verbose_name='Заказ',
        help_text='Ссылка на заказ',
        to=Order,
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = 'Контакт анонима'
        verbose_name_plural = 'Контакты анонимных пользователей'

    def __str__(self):
        return f'{self.email}'

    def __repr__(self):
        return f'[{self.pk}] email={self.email}'
