from functools import wraps

from django.conf import settings
from django.http import HttpRequest


def convert_kwargs_to_str(params: tuple):
    """
    Декоратор, который приводит указанные параметры функции к типу `str`.

    Параметры:
        params  - кортеж, содержащий список именованных параметров, к которым необходимо применить функцию преобразования типов.
    """
    def decorator(func):
        
        @wraps(func)
        def wrapper(*args, **kwargs):
            for key, value in kwargs.items():
                if params is None or key in params:
                    kwargs[key] = str(value)

            return func(*args, **kwargs)

        return wrapper

    return decorator


class SessionCart:
    """
        Инкапсулирует логику Корзины товаров, хранящуюся в сессии пользователя.
    """

    def __init__(self, request: HttpRequest):
        self.session = request.session
        self.cart = self.session.get(settings.CART_SESSION_ID, dict())

    @convert_kwargs_to_str(('product_id', 'warehouse_id'))
    def add_item(self, product_id: str, warehouse_id: str, quantity: int):
        """
        Кладёт товар в корзину.
        Если товар уже есть в корзине, то увеличивает его количество на соответствующее значение.
        """

        if quantity <= 0:
            raise ValueError('Количество товара, которое необходимо положить в корзину, должно быть больше нуля.')

        product_warehouses = self.cart.get(product_id)

        if product_warehouses is None:
            self.cart[product_id] = {warehouse_id: quantity}
        else:
            if product_warehouses.get(warehouse_id) is None:
                self.cart[product_id][warehouse_id] = quantity
            else:
                self.cart[product_id][warehouse_id] += quantity

        self.save()

    @convert_kwargs_to_str(('product_id', 'warehouse_id'))
    def change_item(self, product_id: str, warehouse_id: str, quantity: int):
        """
        Изменяет кол-во товара в корзине.
        """

        if quantity <= 0:
            raise ValueError(f'Невозможно изменить товар в корзине. Количество товара в корзине должно быть > 0. Переданное количество= {quantity}.')

        product_warehouses = self.cart.get(product_id)

        if product_warehouses is None:
            raise ValueError(f'Ошибка при изменении количества товара: В корзине отсутствует товар с {product_id=}')

        if product_warehouses.get(warehouse_id) is None:
            raise ValueError(f'Ошибка при изменении количества товара: В корзине отсутствует товар с {warehouse_id=}')

        self.cart[product_id][warehouse_id] = quantity
        self.save()

    @convert_kwargs_to_str(('product_id', 'warehouse_id'))
    def delete_item(self, product_id: str, warehouse_id: str):
        """ Удаляет товар по складу из корзины. """

        product_warehouses = self.cart.get(product_id)

        if product_warehouses is None:
            raise ValueError(f'Ошибка при удалении товара: В корзине отсутствует товар с {product_id=}')

        if product_warehouses.get(warehouse_id) is None:
            raise ValueError(f'Ошибка при удалении товара: В корзине отсутствует товар с {warehouse_id}')

        del self.cart[product_id][warehouse_id]

        if not self.cart[product_id]:
            del self.cart[product_id]

        self.save()

    def clear_cart(self):
        """ Очищает корзину. """

        self.cart = dict()
        self.save()

    def cart_to_dict(self) -> dict[str, list[dict]]:
        """ Возвращает корзину """

        cart = list()

        for product_id, warehouses in self.cart.items():
            for warehouse_id, quantity in warehouses.items():
                cart.append({
                            'product_id': product_id,
                            'warehouse_id': warehouse_id,
                            'quantity': quantity
                            })

        return {'products': cart}

    def save(self):
        """ Обновление сессии cart. """

        self.session[settings.CART_SESSION_ID] = self.cart
