from typing import TypedDict, List

from rest_framework.response import Response
from rest_framework.utils.urls import remove_query_param, replace_query_param
from rest_framework.pagination import PageNumberPagination


class CustomPagination(PageNumberPagination):
    """
    Собственный способ пагинации.
    Необходимо выводить 2 страницы (в обе стороны) от текущей + первую и последнюю страницу.
    Эта логика прописана в формировании списка 'page_links'
    """

    def get_paginated_response(self, data) -> Response:

        class PageLink(TypedDict):
            page_number: int
            page_url: str

        class PaginationData(TypedDict):
            page_links: List[PageLink]
            total_pages: int
            current_page_number: int
            total_objects: int
            results: str

        def page_number_to_url(page_number):
            base_url = self.request.path

            if page_number == 1:
                return remove_query_param(base_url, self.page_query_param)
            else:
                return replace_query_param(base_url, self.page_query_param, page_number)

        page_links = {
            page_number: page_number_to_url(page_number) for page_number in range(1, self.page.paginator.num_pages + 1)
        }

        page_links = [{
            'page_number': page_number, 'page_url': page_url} for page_number, page_url
             in page_links.items()
             if abs(page_number - self.page.number) < 3
             or (page_number == 1 or page_number == self.page.paginator.num_pages)
        ]

        pagination_data: PaginationData = {
            'page_links': page_links,
            'total_pages': self.page.paginator.num_pages,
            'current_page_number': self.page.number,
            'total_objects': self.page.paginator.count,
            'results': data
        }

        return Response(pagination_data)
