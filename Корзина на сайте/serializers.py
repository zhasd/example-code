from rest_framework import serializers

from catalog.models import Product, Warehouse

from catalog.serializers import ProductImageSerializer


class ProductSerializer(serializers.ModelSerializer):
    """ Описывает сериализацию данных для модели Product. """

    images = ProductImageSerializer(many=True, read_only=True)

    class Meta:
        model = Product

        fields = ['id', 'title', 'unit', 'price', 'images']


class WarehouseSerializer(serializers.ModelSerializer):
    """ Описывает сериализацию данных для модели Warehouse. """

    class Meta:
        model = Warehouse

        fields = ['id', 'address']


class CartSerializer(serializers.Serializer):
    """ Описывает сериализацию данных для корзины. """

    product = ProductSerializer(read_only=True)
    warehouse = WarehouseSerializer(read_only=True)
    quantity = serializers.IntegerField(read_only=True)
    amount = serializers.DecimalField(max_digits=12, decimal_places=2)
