from collections import defaultdict
from functools import lru_cache

from catalog.models import Product, Brand


@lru_cache(maxsize=32, typed=False)
def get_features_for_product(product_id: int) -> list:

    features = Product.objects.prefetch_related(
        'features',
        'stocks',
    ).filter(
        pk=product_id,
        stocks__available=True,
    ).values(
        'features__id',
        'features__title',
        'features__value',
        'features__unit',
    ).distinct()

    # Group feature values to feature titles
    features_group = defaultdict(list)
    for feature in features:
        feature_id = feature['features__id']
        feature_title = feature['features__title']
        feature_value = feature['features__value']
        feature_unit = feature['features__unit']
        
        if feature_id is None:
            continue

        feature_item = ({'feature_id': feature_id,
                         'feature_name': feature_value,
                         'feature_unit': feature_unit})

        features_group[feature_title].append(feature_item)

    result = []

    for key, value in features_group.items():
        result.append({'features__title': key,
                       'features__params': value})

    return result


@lru_cache(maxsize=32, typed=False)
def get_features_for_subcategory(subcategory_slug: str) -> list:

    features = Product.objects.prefetch_related(
        'features',
        'stocks',
    ).filter(
        subcategories__slug=subcategory_slug,
        stocks__available=True,
    ).values(
        'features__id',
        'features__title',
        'features__value',
        'features__unit',
    ).distinct()

    # Group feature values to feature titles
    features_group = defaultdict(list)
    for feature in features:
        feature_id = feature['features__id']
        feature_title = feature['features__title']
        feature_value = feature['features__value']
        feature_unit = feature['features__unit']

        if feature_id is None:
            continue

        feature_item = ({'feature_id': feature_id,
                         'feature_name': feature_value,
                         'feature_unit': feature_unit})

        features_group[feature_title].append(feature_item)

    result = []

    for key, value in features_group.items():
        result.append({'features__title': key,
                       'features__params': value})

    return result